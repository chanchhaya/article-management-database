package com.chhaya.articlemanagementsystem.repository.provider;

import org.apache.ibatis.jdbc.SQL;

import com.chhaya.articlemanagementsystem.model.ArticleFilter;

public class ArticleProvider {
	
	public String findAllFilter (ArticleFilter articleFilter) {
		
		return new SQL() {{
			
			SELECT("a.id, a.title AS name, a.description, a.author, a.thumnail, a.created_date, a.category_id, c.name AS category_name");
			FROM("tb_articles AS a");
			INNER_JOIN("tb_categories AS c ON a.category_id = c.id");

			if(articleFilter.getName() != null) {
				WHERE("a.title ILIKE '%' || #{name} || '%'");
			}
			
			if(articleFilter.getCate_id() != null) {
				WHERE("a.category_id = #{cate_id}");
			}
			ORDER_BY("a.id ASC");
			
		}}.toString();
	}
}
