package com.chhaya.articlemanagementsystem.repository.category;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.chhaya.articlemanagementsystem.model.Category;

@Repository
public interface CategoryRepository {
	//void add(Category category);
	
	@Select("SELECT id, name FROM tb_categories WHERE id=#{id}")
	@Results({
		@Result(property="id", column="id"),
		@Result(property="name", column="name")
	})
	public Category find(int id);
	
	@Select("SELECT id, name FROM tb_categories ORDER BY id ASC")
	@Results({
		@Result(property="id", column="id"),
		@Result(property="name", column="name")
	})
	public List<Category> findAll();
	
	@Insert("INSERT INTO tb_categories(name) VALUES(#{name})")
	public void add(Category category);
	
	@Update("UPDATE tb_categories SET name=#{name} WHERE id=#{id}")
	public void update(Category category);
	
	@Delete("DELETE FROM tb_categories WHERE id=#{id}")
	public void delete(int id);
}
