package com.chhaya.articlemanagementsystem.service.category;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chhaya.articlemanagementsystem.model.Category;
import com.chhaya.articlemanagementsystem.repository.category.CategoryRepository;

@Service
public class CategoryServiceImpl implements CategoryService{
	
	private CategoryRepository categoryRepo;
	
	@Autowired
	public void setCategoryRepository(CategoryRepository categoryRepository) {
		this.categoryRepo = categoryRepository;
	}

	@Override
	public Category find(int id) {
		return categoryRepo.find(id);
	}

	@Override
	public List<Category> findAll() {
		return categoryRepo.findAll();
	}
	
	@Override
	public void add(Category category) {
		categoryRepo.add(category);
	}
	
	@Override
	public void update(Category category) {
		categoryRepo.update(category);
	}
	
	@Override
	public void delete(int id) {
		categoryRepo.delete(id);
	}
	


	
	
}
