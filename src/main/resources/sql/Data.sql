insert into tb_categories (name) values ('Spring');
insert into tb_categories (name) values ('Java');
insert into tb_categories (name) values ('Web');
insert into tb_categories (name) values ('Korean');
insert into tb_categories (name) values ('Database System');

insert into tb_articles (articlehash,title,description,author,thumnail,created_date,category_id)
    values ('#001','Object Oriented Programming','OOP concept','Tem Channat','avatar.png','18.06.13',2);
insert into tb_articles (articlehash,title,description,author,thumnail,created_date,category_id)
    values ('#002','Spring Framework','Basic spring framework','Peng Chhaileng','avatar5.png','18.06.13',1);
insert into tb_articles (articlehash,title,description,author,thumnail,created_date,category_id)
    values ('#003','Web Design','HTML, CSS, Javascript, jQuery','Mom Reksmey','avatar2.png','18.06.13',1);
insert into tb_articles (articlehash,title,description,author,thumnail,created_date,category_id)
    values ('#004','Sokang Book','Level 1','Chan Thon','avatar4.png','18.06.13',1);
insert into tb_articles (articlehash,title,description,author,thumnail,created_date,category_id)
    values ('#005','Sokang Book','Level 1','Houn Sorphea','avatar3.png','18.06.13',1);
insert into tb_articles (articlehash,title,description,author,thumnail,created_date,category_id)
    values ('#006','Sokang Book','Level 1','Houn Sorphea','avatar3.png','18.06.13',1);
insert into tb_articles (articlehash,title,description,author,thumnail,created_date,category_id)
    values ('#007','Sokang Book','Level 1','Houn Sorphea','avatar3.png','18.06.13',1);
insert into tb_articles (articlehash,title,description,author,thumnail,created_date,category_id)
    values ('#008','Sokang Book','Level 1','Houn Sorphea','avatar3.png','18.06.13',1);

