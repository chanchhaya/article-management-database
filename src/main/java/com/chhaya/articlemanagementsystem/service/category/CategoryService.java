package com.chhaya.articlemanagementsystem.service.category;

import java.util.List;


import com.chhaya.articlemanagementsystem.model.Category;


public interface CategoryService {
	Category find(int id);
	List<Category> findAll();
	void add(Category category);
	void update(Category category);
	void delete(int id);
}
