create table tb_categories(
  id int primary key auto_increment,
  name varchar(25)
);
create table tb_articles(
    id int primary key auto_increment,
    articlehash varchar(50),
    title varchar(50),
    description varchar(100),
    author varchar(50),
    thumnail varchar(100),
    created_date varchar(15),
    category_id int references tb_categories(id) on DELETE cascade
);