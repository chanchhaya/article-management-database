package com.chhaya.articlemanagementsystem.model;

public class ArticleFilter {
	private String name;
	private Integer cate_id;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getCate_id() {
		return cate_id;
	}
	public void setCate_id(Integer cate_id) {
		this.cate_id = cate_id;
	}
	@Override
	public String toString() {
		return "ArticleFilter [name=" + name + ", cate_id=" + cate_id + "]";
	}
	
	
}
