package com.chhaya.articlemanagementsystem.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.chhaya.articlemanagementsystem.model.Article;
import com.chhaya.articlemanagementsystem.model.ArticleFilter;
import com.chhaya.articlemanagementsystem.service.article.ArticleService;
import com.chhaya.articlemanagementsystem.service.category.CategoryService;

@Controller
@PropertySource("classpath:ams.properties")
public class ArticleController {
	
	private ArticleService articleService;
	private CategoryService categoryService;
	
	@Value("${file.upload.server.path}")
	String serverPath;
	
	@Autowired
	public void setArticleService(ArticleService articleService) {
		this.articleService = articleService;
	}
	@Autowired
	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

	@GetMapping("/article")
	public String article(ArticleFilter filter, ModelMap m) {
		List<Article> articles = articleService.findAllFilter(filter);
		System.out.println(filter);
		System.out.println(articles);
		m.addAttribute("articles", articles);
		m.addAttribute("filter", filter);
		m.addAttribute("categories", categoryService.findAll());
		return "article/article-detail";
	}
	
	@GetMapping("/article/add")
	public String add(ModelMap modelMap) {
		modelMap.addAttribute("article", new Article());
		modelMap.addAttribute("categories", categoryService.findAll());
		modelMap.addAttribute("formAdd", true);
		return "article/article-add";
	}
	
	@GetMapping("/article/delete/{id}")
	public String delete(@PathVariable("id") int id) {
		articleService.delete(id);
		return "redirect:/article";
	}
	
	@GetMapping("/article/edit/{id}")
	public String edit(@PathVariable int id, ModelMap modelMap) {
		modelMap.addAttribute("article", articleService.find(id));
		modelMap.addAttribute("categories", categoryService.findAll());
		modelMap.addAttribute("formAdd", false);
		return "article/article-add";
	}
	
	@GetMapping("/article/view/{id}")
	public String viewArticle(@PathVariable int id, ModelMap modelMap) {
		modelMap.addAttribute("article", articleService.find(id));
		modelMap.addAttribute("categories", categoryService.findAll());
		return "article/article-view";
	}
	
	@PostMapping("/article/add")
	public String saveArticle(@Valid @ModelAttribute Article article, BindingResult result, ModelMap modelMap, @RequestParam("file") MultipartFile file) {
		
		if(result.hasErrors()) {
			System.out.println(result);
			modelMap.addAttribute("article", article);
			modelMap.addAttribute("formAdd", true);
			return "article/article-add";
		}
		
		if(!file.isEmpty()) {
			String fileName = UUID.randomUUID() + "." + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
			try {
				Files.copy(file.getInputStream(), Paths.get(serverPath, fileName));
				article.setThumnail(fileName);
			} catch (IOException e) { 
				e.printStackTrace();
			}
		}
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String date = simpleDateFormat.format(new Date());
		article.setCreatedDate(date);
		
		articleService.add(article);
		return "redirect:/article";
	}
	
	@PostMapping("/article/edit")
	public String saveEdit(@ModelAttribute Article article, @RequestParam("file") MultipartFile file) {
		
		if(!file.isEmpty()) {
			String fileName = UUID.randomUUID() + "." + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
			try {
				Files.copy(file.getInputStream(), Paths.get(serverPath, fileName));
				article.setThumnail(fileName);
			} catch (IOException e) { 
				e.printStackTrace();
			}
		}
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String date = simpleDateFormat.format(new Date());
		article.setCreatedDate(date);
		
		articleService.update(article);
		return "redirect:/article";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
