package com.chhaya.articlemanagementsystem.configuration;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class DatabaseConfiguration {

	@Bean
	@Profile("database")
	public DataSource production(
			
			) {
		DriverManagerDataSource db = new DriverManagerDataSource();
		db.setDriverClassName("org.postgresql.Driver");
		db.setUrl("jdbc:postgresql://localhost:5432/articledbs");
		db.setUsername("postgres");
		db.setPassword("root");
		return db;
	}
	
	
	@Bean
	@Profile("memory")
	public DataSource development() {
		EmbeddedDatabaseBuilder db = new EmbeddedDatabaseBuilder();
		db.setType(EmbeddedDatabaseType.H2);
		db.addScript("sql/createTable.sql");
		db.addScript("sql/Data.sql");
		return db.build();
	}
	
}
