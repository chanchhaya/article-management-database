package com.chhaya.articlemanagementsystem.service.article;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chhaya.articlemanagementsystem.model.Article;
import com.chhaya.articlemanagementsystem.model.ArticleFilter;
import com.chhaya.articlemanagementsystem.repository.article.ArticleRepository;

@Service
public class ArticleServiceImpl implements ArticleService{

	private ArticleRepository articleRepo;
	
	@Autowired
	public void setArticleRepo(ArticleRepository articleRepo) {
		this.articleRepo = articleRepo;
	}

	@Override
	public void add(Article article) {
		
		articleRepo.add(article);
	}

	@Override
	public Article find(int id) {
		return articleRepo.find(id);
	}
	
	@Override
	public List<Article> findAll() {
		return articleRepo.findAll();
	}
	
	@Override
	public void delete(int id) {
		articleRepo.delete(id);
	}
	
	@Override
	public void update(Article article) {
		articleRepo.update(article);
	}

	@Override
	public List<Article> findAllFilter(ArticleFilter articleFilter) {
		return articleRepo.findAllFiter(articleFilter);
	}
	

}
