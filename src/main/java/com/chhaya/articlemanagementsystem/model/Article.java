package com.chhaya.articlemanagementsystem.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class Article {
	
	// attributes in article
	@NotNull
	private int id;
	@NotBlank
	@Size(min=5, max=100)
	private String name;
	@NotBlank
	private String description;
	@NotBlank
	@Size(min=5, max=20)
	private String author;
	private String thumnail;
	private String createdDate;
	private Category category;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getThumnail() {
		return thumnail;
	}
	public void setThumnail(String thumnail) {
		this.thumnail = thumnail;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public Article(int id, String name, String description, String author, String thumnail, String createdDate,
			Category category) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.author = author;
		this.thumnail = thumnail;
		this.createdDate = createdDate;
		this.category = category;
	}
	
	public Article() {}

	@Override
	public String toString() {
		return "Article [id=" + id + ", name=" + name + ", description=" + description + ", author=" + author
				+ ", thumnail=" + thumnail + ", createdDate=" + createdDate + ", category=" + category + "]";
	}
	public Article(int id, String name, String description, String author, String thumnail, String createdDate) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.author = author;
		this.thumnail = thumnail;
		this.createdDate = createdDate;
	}
	
	
	
	
	
	
	
	
	
	
}
