package com.chhaya.articlemanagementsystem.controller;


import java.util.List;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.chhaya.articlemanagementsystem.model.Category;
import com.chhaya.articlemanagementsystem.service.category.CategoryService;

@Controller
public class CategoryController {
	
	private CategoryService categoryService;
	
	@Autowired
	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	
	@GetMapping("/category")
	public String category(ModelMap m) {
		List<Category> categories = categoryService.findAll();
		System.out.println(categories);
		m.addAttribute("categories", categories);
		return "category/category-detail";
	}
	
	
	@GetMapping("/category/add")
	public String add(ModelMap modelMap) {	
		modelMap.addAttribute("category", new Category());
		modelMap.addAttribute("formAdd", true);
		return "category/category-add";
	}
	@PostMapping("/category/add")
	public String saveArticle(@Valid @ModelAttribute Category category, BindingResult result, ModelMap modelMap) {
		
		if(result.hasErrors()) {
			System.out.println(result);
			modelMap.addAttribute("category", category);
			modelMap.addAttribute("formAdd", true);
			return "category/category-add";
		}
		categoryService.add(category);
		return "redirect:/category";
	} 
	
	
	@GetMapping("/category/edit/{id}") 
	public String edit(@PathVariable int id, ModelMap modelMap) {
		modelMap.addAttribute("category", categoryService.find(id));
		modelMap.addAttribute("formAdd", false);
		return "category/category-add";
	}
	@PostMapping("/category/edit")
	public String saveEdit(@ModelAttribute Category category) {
		categoryService.update(category);
		return "redirect:/category";
	}
	
	
	@GetMapping("/category/delete/{id}")
	public String delete(@PathVariable("id") int id) {
		categoryService.delete(id);
		return "redirect:/category";
	}
}
