package com.chhaya.articlemanagementsystem.service.article;

import java.util.List;

import com.chhaya.articlemanagementsystem.model.Article;
import com.chhaya.articlemanagementsystem.model.ArticleFilter;

public interface ArticleService {
	
	void add(Article article);
	Article find(int id);
	List<Article> findAll();
	void delete(int id);
	void update(Article article);
	List<Article> findAllFilter(ArticleFilter articleFilter);
}
