package com.chhaya.articlemanagementsystem.repository.article;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.chhaya.articlemanagementsystem.model.Article;
import com.chhaya.articlemanagementsystem.model.ArticleFilter;
import com.chhaya.articlemanagementsystem.repository.provider.ArticleProvider;

@Repository
public interface ArticleRepository {
	
	// abstract methods
	
	@Insert("INSERT INTO tb_articles(title, description, author, thumnail, created_date, category_id)"
			+ " VALUES (#{name},#{description},#{author},#{thumnail},#{createdDate},#{category.id})")
	public void add(Article article);
	
	@Select("SELECT a.id, a.title AS name, a.description, a.author, a.thumnail, a.created_date, a.category_id, c.name AS category_name"
			+ " FROM tb_articles AS a INNER JOIN tb_categories AS c"
			+ " ON a.category_id = c.id"
			+ " WHERE a.id = #{id}")
	@Results({
		@Result(property="id", column="id"),
		@Result(property="name", column="name"),
		@Result(property="description", column="description"),
		@Result(property="author", column="author"),
		@Result(property="thumnail", column="thumnail"),
		@Result(property="createdDate", column="created_date"),
		@Result(property="category.id", column="category_id"),
		@Result(property="category.name", column="category_name")
	})
	public Article find(int id);
	
	
	@Select("SELECT a.id, a.title AS name, a.description, a.author, a.thumnail, a.created_date, a.category_id, c.name AS category_name"
			+ " FROM tb_articles AS a"
			+ " INNER JOIN tb_categories AS c"
			+ " ON a.category_id = c.id"
			+ " ORDER BY a.id ASC")
	@Results({
		@Result(property="createdDate", column="created_date"),
		@Result(property="category.id", column="category_id"),
		@Result(property="category.name", column="category_name")
	})
	public List<Article> findAll();
	
	@Delete("DELETE FROM tb_articles WHERE id=#{id}")
	public void delete(int id);
	
	@Update("UPDATE tb_articles SET title=#{name}, description=#{description}, author=#{author}, thumnail=#{thumnail}, created_date=#{createdDate}, category_id=#{category.id}"
			+ " WHERE id=#{id}")
	public void update(Article article);
	
	@SelectProvider(method="findAllFilter", type=ArticleProvider.class)
	@Results({
		@Result(property="createdDate", column="created_date"),
		@Result(property="category.id", column="category_id"),
		@Result(property="category.name", column="category_name")
	})
	public List<Article> findAllFiter(ArticleFilter filter);
}
